# guard-duty

Terraform module to setup AWS GuardDuty.

This module is forked from a [similar module](https://github.com/cmdlabs/terraform-aws-guardduty) with the difference that this module's interface is inline with the other
 modules of this collection, and it offers the ability to pass in resources like S3 bucket instead of creating them within the module.
